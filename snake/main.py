#!/usr/bin/env python3
# surprise tools to help us later
import os
import sys
import random

# constant directions the snake can move
# these are treated as mathematical vectors
UP = (0, 1)
DOWN = (0, -1)
LEFT = (-1, 0)
RIGHT = (1, 0)

# defining our snake
class Snake:
    # constructor to init the body and direction
    def __init__(self, init_body, init_direction):
        self.body = init_body
        self.direction = init_direction

    # update the snakes position based on a new point
    def take_step(self, position):
        self.body = self.body[1:] + [position]

    # grow the snake based on a new point
    def grow(self, position):
        self.body = self.body + [position]

    # easy access to setting the direction
    def set_direction(self, direction):
        self.direction = direction

    # returns the point containing the head
    def head(self):
        return self.body[-1]


# probably could be written without this
class Apple:
    # give the apple a location
    def __init__(self, location):
        self.location = location


# we will make an instance of this class to play the game
class Game:
    # init basic values for things we will need
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.snake = Snake([(0, 0), (1, 0), (2, 0), (3, 0), (4, 0)], RIGHT)
        self.score = 0

    # creating an x, y grid system and setting points
    # equal to values corresponding to the types of spaces
    # we can have (None = none, 0 = body, 1 = head, 2 = apple)
    def board_matrix(self):
        # the way the grid is generated is important, by doing it in this way
        # each list of none values is unique. if this matrix were generated in
        # other possible ways python could think that the list of nones was the
        # same list of nones, and modifying one would modify them all
        board = [[None for _ in range(self.height)] for _ in range(self.width)]

        for point in self.snake.body:
            x = point[0]
            y = point[1]

            if self.snake.head() == point:
                board[x][y] = 1
            else:
                board[x][y] = 0

        board[self.apple.location[0]][self.apple.location[1]] = 2

        return board

    # generating a new location for the apple randomly
    # for as random as random can actually be
    def gen_apple(self):
        # tries to generate a new location until the apple
        # isn't spawned inside the snake
        while True:
            new_location = (
                random.randint(0, self.width - 1),
                random.randint(0, self.height - 1),
            )
            if new_location not in self.snake.body:
                break

        self.apple = Apple(new_location)

    # does vector addition of a position tuple with a direction tuple
    # also accounts for going out of bounds and looping back around
    def new_position(self, position, step):
        return (
            (position[0] + step[0]) % self.width,
            (position[1] + step[1]) % self.height,
        )

    # what actually prints things to the screen
    def render(self):
        # creating a board to read from
        matrix = self.board_matrix()

        # little one-liner to clear the terminal on all oses
        os.system("cls" if os.name == "nt" else "clear")

        # printing the top border
        print("+-+".replace("-", "-" * self.width))

        # this function steps through each x,y coord
        # and prints to the terminal a character corresponding to its value
        #
        # there's probably a more efficient way to do this but I haven't
        # looked into it to much
        for y in range(0, self.height):
            print("|", end="")

            for x in range(0, self.width):
                # this is the single most infuriating line of this program
                # originally the coordinate system started in the top left
                # because this line never worked, but by doing the height of
                # the gamespace - 1 - y then we are able to print the board from
                # top to bottom despite the system starting in the bottom left
                item = matrix[x][self.height - 1 - y]
                if item == None:
                    print(" ", end="")
                elif item == 0:
                    print("0", end="")
                elif item == 1:
                    print("X", end="")
                elif item == 2:
                    print("a", end="")

            print("|")

        # printing the bottom border
        print("+-+".replace("-", "-" * self.width))


# main function to run the game when the file is run
if __name__ == "__main__":
    game = Game(20, 10)
    game.gen_apple()
    game.render()
    while True:
        # getting an input from the user
        dir = input().strip()

        # if its w, s, a, d, then we change the snake direction
        # if its none of these then it keeps moving in
        # whatever direction is was already going
        if dir == "w" and game.snake.direction != DOWN:
            game.snake.direction = UP
        elif dir == "s" and game.snake.direction != UP:
            game.snake.direction = DOWN
        elif dir == "a" and game.snake.direction != RIGHT:
            game.snake.direction = LEFT
        elif dir == "d" and game.snake.direction != LEFT:
            game.snake.direction = RIGHT

        # get the next point the snakes head should move to
        next_position = game.new_position(game.snake.head(), game.snake.direction)
        # if the new position is inside itself, game over
        if next_position in game.snake.body:
            print(f"Game Over! You scored {game.score} points")
            break

        # if the new position is inside an apple, uptick the score,
        # grow the snake, and generate a new apple
        # if not, then take a step forward
        if next_position == game.apple.location:
            game.score += 1
            game.snake.grow(next_position)
            game.gen_apple()
        else:
            game.snake.take_step(next_position)

        # finally print the game to the terminal
        game.render()
